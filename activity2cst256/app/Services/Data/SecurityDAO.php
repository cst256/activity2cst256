<?php
namespace App\Services\Data;
use App\Models\UserModel;
class SecurityDAO
{
    public function findByUser(UserModel $model){
            $connection = mysqli_connect("localhost", "root", "root", "activity2");
            $sql = "SELECT * FROM users WHERE username = ? and PASSWORD = ?";
            $stmt = $connection->prepare($sql);
            $username = $model->getUsername();
            $password = $model->getPassword();
            $stmt->bind_param("ss", $username, $password);
            
            //Execute query
            $stmt->execute();
            $results = $stmt->get_result();
            
            return $results->num_rows > 0;
            
    }
}

