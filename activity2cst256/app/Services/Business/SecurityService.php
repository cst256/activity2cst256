<?php
namespace App\Services\Business;

use App\Models\UserModel;
use App\Services\Data\SecurityDAO;

class SecurityService
{
    public function login(UserModel $userModel){
         $loginHandler = new SecurityDAO();
         
         //Attempt to login
         return $loginHandler->findByUser($userModel);
    }
}

